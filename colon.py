#! /usr/bin/env python
"""
A skeleton python script which reads from an input file,
writes to an output file and parses command line arguments
"""
from __future__ import print_function
import sys
import argparse

def main():
    parser = argparse.ArgumentParser(description=__doc__)

    parser.add_argument(
        "input", nargs="?", default="-",
        metavar="INPUT_FILE", type=argparse.FileType("r"),
        help="path to the input file (read from stdin if omitted)")

    parser.add_argument(
        "output", nargs="?", default="-",
        metavar="OUTPUT_FILE", type=argparse.FileType("w"),
        help="path to the output file (write to stdout if omitted)")

    args = parser.parse_args()

    lineno = 1
    for line in args.input:
        if lineno > 537:
            if line[0].isalpha():
                thesplit = line.rstrip().split(" ", 1)
                if (len(thesplit) >= 2):
                    print(thesplit[0] + ":" + thesplit[1], file=args.output)
                else:
                    print(thesplit[0] + ":", file=args.output)
            else:
                print(line.rstrip(), file=args.output)
        else:
            print(line.rstrip(), file=args.output)
        lineno += 1

if __name__ == "__main__":
    main()
