;******************************************************************************
;*
;*  TX8 8-bit color computer
;*
;*  mount.s
;*    Mount a file system
;*
;* Read partition header and prepare the file system for usage
;*
;******************************************************************************
  .setcpu   "65c02"

.include    "txfs.i"
.include    "txfs_data.i"
.include    "txfs_util.i"
.include    "cf_driver.i"
.include    "macros.i"
.include    "partition.i"

d_ptr       = $00           ; ZP used during boot
s_ptr       = $02           ; Same

.segment    "CODE"
;******************************************************************************
;* Mount partition
;*  ACC - Holds partition to load
;*    X -
;*    Y -
;******************************************************************************
txfs_mount:
  ldx   #<part_sect         ; Prepare to fetch partition base information
  ldy   #>part_sect
  jsr   txfs_load_partition ; Go get information

  ; Read boot block from partition
  load_sect part_sect + txfs_pi::first_sect
  lda   part_sect + txfs_pi::first_sect
  sta   cflba0
  lda   part_sect + txfs_pi::first_sect+1
  sta   cflba1
  lda   part_sect + txfs_pi::first_sect+2
  sta   cflba2
  lda   part_sect + txfs_pi::first_sect+3
  sta   cflba3
  jsr   cf_read_sector      ; Ge read boot sector of partition

  ; Copy boot information to system structure
  ldp   d_ptr, part_info
  ldp   s_ptr, disk_buffer
  ldy   #0
  ldx   #<part_info_len
tm_001:
  lda   (s_ptr),y
  sta   (d_ptr),y
  iny
  dex
  bne   tm_001

  ; Adjust all partition pointers based on partition
  ldp   d_ptr, part_info + txfs_partition::inode_bmp_1
  ldp   s_ptr, part_sect + txfs_pi::first_sect
  jsr   add_LBA32
  ldp   s_ptr, part_sect + txfs_partition::first_sect
  jsr   add_LBA32

  ldp   d_ptr, part_info + txfs_partition::data_bmp_1
  ldp   s_ptr, part_sect + txfs_pi::first_sect
  jsr   add_LBA32
  ldp   s_ptr, part_sect + txfs_partition::first_sect
  jsr   add_LBA32

  ldp   d_ptr, part_info + txfs_partition::inodes_1
  ldp   s_ptr, part_sect + txfs_pi::first_sect
  jsr   add_LBA32
  ldp   s_ptr, part_sect + txfs_partition::first_sect
  jsr   add_LBA32

  ldp   d_ptr, part_info + txfs_partition::inodes_1
  ldp   s_ptr, part_sect + txfs_pi::first_sect
  jsr   add_LBA32
  ldp   s_ptr, part_sect + txfs_partition::first_sect
  jsr   add_LBA32

  rts

;******************************************************************************
;* Copy LBA information
;*  ACC - Holds partition to load
;*    X -
;*    Y -
;******************************************************************************
