;******************************************************************************
;*
;*  TX8 8-bit color computer
;*
;*  partition.s
;*    Load a detected disks super block and create partition table.
;*
;* Read super block and build partition table
;* With a 2GB memory card there is room on the card to support 64 partitions
;* This is much more than we will every need for any kind of use case.
;* A 256MByte card have room for 8 partitions which seem more suitable for
;* our application.
;*
;******************************************************************************
  .setcpu   "65c02"

.include    "txfs.i"
.include    "cf_driver.i"
.include    "macros.i"

.export     txfs_load_partition

d_ptr       = $00           ; ZP used during boot
s_ptr       = $02           ; Same

.segment    "DATA"

; Partition table
part_tab:  .tag    txfs_sb
part_sect0: .tag    txfs_pi
part_sect1: .tag    txfs_pi
part_sect2: .tag    txfs_pi
part_sect3: .tag    txfs_pi
part_sect4: .tag    txfs_pi
part_sect5: .tag    txfs_pi
part_sect6: .tag    txfs_pi
part_sect7: .tag    txfs_pi

part_len  = .sizeof(part_tab) + .sizeof(part_sect0) * 8

;******************************************************************************
;* Load partition information into mount table
;*   Mount the specified partition
;*   ACC - the partition to mount
;*     X - low ptr byte of return structure
;*     Y - high ptr byte of return structure
;******************************************************************************
txfs_load_partition:
  stx   d_ptr
  sty   d_ptr+1

  asl   a                   ; Multiply by 8 to get offset into part table
  asl   a
  asl   a
  tay

  ldp   s_ptr, part_sect0
  ldy   #0
  ldx   #8
tm_001:
  lda   (s_ptr),y
  sta   (d_ptr),y
  iny
  dex
  bne   tm_001

  rts

;******************************************************************************
;* Read partition information
;*   Read partition information and store in system partition table.
;******************************************************************************
txfs_partition_init:
  jsr   read_super_block

  ldp   d_ptr, part_tab     ; Set up copy pointers
  ldp   s_ptr, disk_buffer

  ; Copy disk buffer to system partition structure
  ldy   #00
  ldx   part_len
tpr_001:
  lda   (s_ptr),y
  sta   (d_ptr),y
  iny
  dex
  bne   tpr_001

  rts

;******************************************************************************
;* Read super block
;*   Simply reads the first sector of the disk.
;******************************************************************************
read_super_block:
  lda   #0
  sta   cflba3
  sta   cflba2
  sta   cflba1
  sta   cflba0
  jsr   cf_read_sector

  rts
