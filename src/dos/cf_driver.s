;******************************************************************************
;*
;*  TX8 8-bit color computer
;*
;*  cf_driver.s
;*    Low level compact flash card drivers for the TX8
;*    computer.
;*
;******************************************************************************
  .setcpu   "65c02"

  .export   cf_info
  .export   cf_init
  .export   cf_set_lba
  .export   cf_read_sector
  .export   cf_write_sector

  .export   dat_ptr: zeropage
  .export   cf_error: zeropage

  .export   cflba3
  .export   cflba2
  .export   cflba1
  .export   cflba0

;******************************************************************************
;* cf regs
;******************************************************************************
cfbase  =  $fe80
cfreg0  =  cfbase+0         ; data port
cfreg1  =  cfbase+1         ; read: error code, write: feature
cfreg2  =  cfbase+2         ; number of sectors to transfer
cfreg3  =  cfbase+3         ; sector address lba 0 [0:7]
cfreg4  =  cfbase+4         ; sector address lba 1 [8:15]
cfreg5  =  cfbase+5         ; sector address lba 2 [16:23]
cfreg6  =  cfbase+6         ; sector address lba 3 [24:27 (lsb)]
cfreg7  =  cfbase+7         ; read: status, write: command

;******************************************************************************
;* cf commands
;******************************************************************************
cf_cmd_reset  =  $04
cf_cmd_id     =  $ec
cf_cmd_read   =  $20
cf_cmd_write  =  $30

;******************************************************************************
;* start of program
;******************************************************************************
  .setcpu   "65c02"

  .zeropage
dat_ptr:        .res  2
cf_error:       .res  1

  .data
cflba3:         .res  1
cflba2:         .res  1
cflba1:         .res  1
cflba0:         .res  1

  .code

;******************************************************************************
;* initialize cf
;******************************************************************************
cf_init:
  lda   #$04          ; reset command
  sta   cfreg7
  jsr   cf_wait
  lda   #$e0          ; lba3=0, master, mode=lba
  sta   cfreg6
  lda   #$01          ;  8-bit transfers
  sta   cfreg1
  lda   #$ef          ; set feature command
  sta   cfreg7
  jsr   cf_wait
  jmp   cf_check_error

;******************************************************************************
;* Write single sector
;******************************************************************************
cf_write_sector:
  jsr   cf_wait
  jsr   cf_set_lba
  lda   #1
  sta   cfreg2        ; number of sectors to write
  lda   #cf_cmd_write
  sta   cfreg7        ; issue write command
  jmp   cf_write_data ; write data routine

;******************************************************************************
;* Read single sector
;******************************************************************************
cf_read_sector:
  jsr   cf_wait
  jsr   cf_set_lba
  lda   #1
  sta   cfreg2        ; number of sectors to read
  lda   #cf_cmd_read
  sta   cfreg7        ; issue read command
  jmp   cf_read_data  ; read data routine
  rts

;******************************************************************************
;* wait for cf ready
;******************************************************************************
cf_wait:
  lda   cfreg7
  and   #$80          ;  mask out busy flag
  cmp   #$00
  bne   cf_wait
  rts

;******************************************************************************
;* check for cf error
;* Returns carry set if an error
;* occured. If all is good, the
;* carry is cleared.
;******************************************************************************
cf_check_error:
  lda   cfreg7
  and   #$01          ;  mask out error bit
  clc
  cmp   #0
  beq   cf_no_err
  lda   cfreg1        ; Get error
  sta   cf_error      ; and store it
  sec
cf_no_err:
  rts

;******************************************************************************
;* Read data from CF
;*  C clear on OK
;*  error code in A
;*  memory buffer address in dat_ptr. Dat_ptr is not destroyed
;* Implementation notes:
;* This write loop terminates on DRQ bit or if there is an error
;******************************************************************************
cf_read_data:
  phy
  ldy   dat_ptr+1
  phy
  ldy   #0
crd_001:
  jsr   cf_wait
  lda   cfreg7
  and   #$08                ; filter out drq
  cmp   #$08
  bne   cf_read_end
  lda   cfreg0              ; read data byte
  sta   (dat_ptr),y
  iny
  bne   crd_001
  inc   dat_ptr+1
  bra   crd_001             ; Loop again without error checks

  ply
  sty   dat_ptr+1           ; Restore data pointer
  ply
  sec                       ; Set error flag
  rts
cf_read_end:
  ply
  sty   dat_ptr+1           ; Restore data pointer
  ply
  clc                       ; Clear error flag
  rts

;******************************************************************************
;* Write data to CF
;*  C clear on OK
;*  error code in A
;*  memory buffer address in dat_ptr. Dat_ptr is not destroyed
;* Implementation notes:
;* This write loop terminates on DRQ bit or if there is an error
;******************************************************************************
cf_write_data:
  phy
  ldy   dat_ptr+1
  phy
  ldy   #0
cwd_001:
  jsr   cf_wait
  lda   cfreg7
  and   #$08                ; mask out DRQ bit
  cmp   #$08
  bne   cf_write_data_e
  lda   (dat_ptr),y         ; Load data from buffer
  sta   cfreg0              ; write data byte
  iny
  bne   cf_w_001
  inc   dat_ptr+1
cf_w_001:
  jsr   cf_check_error
  beq   cwd_001
  ply
  sty   dat_ptr+1           ; Restore data pointer
  ply
  sec                       ; Set error flag
  rts
cf_write_data_e:
  ply
  sty   dat_ptr+1           ; Restore data pointer
  ply
  clc                       ; Clear error flag
  rts

;******************************************************************************
;* Set lba of CF card.
;* Parameters:
;*    X register points to the file control block that holds the
;*
;******************************************************************************
cf_set_lba:
  lda   cflba0              ;   lba 0
  sta   cfreg3
  lda   cflba1              ;   lba 1
  sta   cfreg4
  lda   cflba2              ;   lba 2
  sta   cfreg5
  lda   cflba3              ;   lba 3
  and   #%00001111          ; filter out lba bits
  ora   #%11100000          ; mode lba, master dev
  sta   cfreg6
  rts

;******************************************************************************
;* Get CF information and return key parameters.
;******************************************************************************
cf_info:
  lda   dat_ptr             ; temporarily store the buffer pointer
  pha
  lda   dat_ptr+1
  pha

  jsr   cf_wait
  lda   #$ec                ; drive id command
  sta   cfreg7
  jsr   cf_read_data        ; go read data
  ; Check if configuration ID is ok

  pla                       ; Restore buffer pointer
  sta   dat_ptr+1
  pla
  sta   dat_ptr

  ldy   #0
  lda   (dat_ptr), y
  cmp   #$4a
  bne   cf_info_fail

  iny
  lda   (dat_ptr), y
  cmp   #$04
  bne   cf_info_fail

  clc
  rts
cf_info_fail:
  sec
  rts

;******************************************************************************
;* storage for sector transfer
;******************************************************************************
