;******************************************************************************
;*
;*  TX8 8-bit color computer
;*
;*  txfs_util.s
;*    A number of different utilities used in txfs.
;*
;******************************************************************************
  .setcpu   "65c02"

.include    "txfs.i"
.include    "txfs_data.i"
.include    "cf_driver.i"
.include    "macros.i"
.include    "partition.i"

.export     add_LBA32
.export     inc_lba_pointer
.export     add_lba32_pointer
.export     add_lba16_pointer

d_ptr       = $00           ; ZP used during boot
s_ptr       = $02           ; Same

;******************************************************************************
;* Add two 32 bit LBA pointers to each other
;*  ACC -
;*    X -
;*    Y -
;* Parameters:
;*  s_ptr   The source operand of the operation
;*  d_ptr   The destination operand of the operation
;*
;*  (d_ptr) = (d_ptr) + (s_ptr)
;******************************************************************************
add_LBA32:
  clc
  ldy   #0
  ldx   #4
alba_001:
  lda   (d_ptr),y
  adc   (s_ptr),y
  sta   (d_ptr),y
  iny
  dex
  bne   alba_001
  rts

;******************************************************************************
;* Increment LBA pointer by one
;*  ACC -
;*    X -
;*    Y -
;* This function increments the current pointer in the driver LBA register
;* by one.
;******************************************************************************
inc_lba_pointer:
  inc   cflba0
  bne   ilp_end
  inc   cflba1
  bne   ilp_end
  inc   cflba2
  bne   ilp_end
  inc   cflba3
ilp_end:
  rts

;******************************************************************************
;* Add 32 bit LBA pointer to the driver LBA register
;*  ACC -
;*    X -
;*    Y -
;* This function adds a 32bit pointer to the current driver LBA registers.
;******************************************************************************
add_lba32_pointer:
  ldy   #0
  clc
  lda   cflba0
  adc   (s_ptr),y
  iny
  sta   cflba0

  lda   cflba1
  adc   (s_ptr),y
  iny
  sta   cflba1

  lda   cflba2
  adc   (s_ptr),y
  iny
  sta   cflba2

  lda   cflba3
  adc   (s_ptr),y
  iny
  sta   cflba3

  rts

;******************************************************************************
;* Add 16 bit LBA pointer to the driver LBA register
;*  ACC -
;*    X -
;*    Y -
;* This function adds a 32bit pointer to the current driver LBA registers.
;******************************************************************************
add_lba16_pointer:
  ldy   #0
  clc
  lda   cflba0
  adc   (s_ptr),y
  iny
  sta   cflba0

  lda   cflba1
  adc   (s_ptr),y
  sta   cflba1

  lda   cflba2
  adc   #0
  sta   cflba2

  lda   cflba3
  adc   #0
  sta   cflba3

  rts
