;******************************************************************************
;*
;*  TX8 8-bit color computer
;*
;*  kbd.i
;*    API for the keyboard driver
;*
;******************************************************************************
    .import   kbd_init
    .import   get_key
    .import   scan_key
