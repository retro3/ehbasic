;**********************************************************
;*
;*  tx8 8-bit computer
;*
;*  .asm
;*  Keyboard controller
;*
;**********************************************************
  .setcpu         "65c02"
  .define         EQU =
  .define         db  .byte
  .define         dw  .word

;**********************************************************
; Exports section
;**********************************************************
  .export         scan_key
  .export         kbd_init
  .export         get_key

;**********************************************************
; VIA (W65C22) registers
;**********************************************************
via_base    = $fe00
portb       = via_base + 0  ; Port B input output register
porta       = via_base + 1  ; Port A input output register
ddrb        = via_base + 2  ; Data direction register port b
ddra        = via_base + 3  ; Data direction register port a
t1cl        = via_base + 4  ; T1 low order latches / counter
t1ch        = via_base + 5  ; T1 high order counter
t1ll        = via_base + 6  ; T1 low order latches
t1lh        = via_base + 7  ; T1 high order latches
t2cl        = via_base + 8  ; T2 low order latches / counter
t2ch        = via_base + 9  ; T2 high order counter
sr          = via_base + 10 ; Shift register
acr         = via_base + 11 ; Auxiliary control register
pcr         = via_base + 12 ; Peripheral control register
ifr         = via_base + 13 ; Interrupt flag register
ier         = via_base + 14 ; Interrupt enable register
porta_nh    = via_base + 15 ; Port A IO register no handshake

rshift_key  = $20
com_key     = $10
lshift_key  = $08
run_key     = $04
ctrl_key    = $02

SYS_FREQ    = 2000000       ; PHI2 clock
TIME_1MS    = (SYS_FREQ / 1000)
OUT_KEYS    =  8            ; max number of keys to detect

;********************************
;* UART Registers
;********************************
UART_BASE     EQU   $FE10

SERIAL_MR1A   EQU   UART_BASE         ;Mode register 1 A
SERIAL_MR2A   EQU   UART_BASE         ;Mode register 2 A
SERIAL_CSRA   EQU   UART_BASE+1       ;Clock select register
SERIAL_CRA    EQU   UART_BASE+2       ;Control register A
SERIAL_ACR    EQU   UART_BASE+4       ;Auxillary control register
SERIAL_TXDATA EQU   UART_BASE+3       ;ACIA WRITE DATA
SERIAL_RXDATA EQU   UART_BASE+3       ;ACIA READ DATA
SERIAL_STATUS EQU   UART_BASE+1       ;ACIA STATUS
SERIAL_WCMD   EQU   UART_BASE+2       ;ACIA COMMAND REGISTER
SERIAL_WCTL   EQU   UART_BASE+3       ;ACIA CONTROL REGISTER
RXRDY         EQU   $01               ;RECEIVE READY
TXRDY         EQU   $04               ;TRANSMIT READY

  .zeropage
ctrl_code:    .res    1
key_row:      .res    1
fnd_rows:     .res    1
rowptr:       .res    2

  .data
temp1:        .res    1
temp2:        .res    1
row_buf:      .res    8
col_buf:      .res    8
old_col_buf:  .res    8
key_buf:      .res    8
old_key_buf:  .res    8
key_out:      .res    OUT_KEYS

  .code
;********************************
;* Put character on serial port
;********************************
outchar:
  pha
pc10:
  lda   SERIAL_STATUS   ;check tx status
  and   #TXRDY          ;rx ready ?
  beq   pc10
  pla
  sta   SERIAL_TXDATA   ;transmit char.
  rts

;********************************
;* PRINT 8 BIT HEX in A
;********************************
output_hex_byte:
  pha
  lsr
  lsr
  lsr
  lsr
  cmp   #10
  bcc   skip1
  adc   #6
skip1:
  adc   #$30
  jsr   outchar

  pla
  and   #$0f
  cmp   #10
  bcc   skip2
  adc   #6
skip2:
  adc   #$30
  jmp   outchar

;**********************************************************
; Initialize Timer 1 to give a 1 mS time base
;**********************************************************
timer1_init:
  lda #$40
  sta acr
  lda #.lobyte(TIME_1MS)
  sta t1ll
  lda #.hibyte(TIME_1MS)
  sta t1lh

  rts

;**********************************************************
; Initialize the VIA for keyboard operation
; Port A is connected to the rows of the keyboard and
; port B is connected to the columns. Port A is driving
; the row output. But only the output that is meant to
; be high (selected). The other pins are left as inputs
; and are being pulled low by pull down resistors.
;**********************************************************
kbd_init:
  lda #$ff
  sta ddra      ; Port A is output
  sta porta_nh  ; Set all outputs hi
  lda #$00
  sta ddrb      ; Port B in input

  ldy #7        ; Clear key scanner buffers
kbd_init_1:
  sta col_buf,y
  sta old_col_buf,y
  dey
  bpl kbd_init_1

  rts

;**********************************************************
; Dig out control keys.
; This function finds the special control keys such as
; Shift, Commodore, control and run stop.
; Input ACC: port B bit value as read directly from port
;         X: Scan row that is being output on port A
; ctrl_code bits:
;   bit7 - No key code
;   bit6 - No key code
;   bit5 - right shift key pressed
;   bit4 - Commodore key pressed
;   bit3 - left shift key pressed
;   bit2 - run key pressed
;   bit1 - ctrl key pressed
;   bit0 - No key code
;
;**********************************************************
chk_control:
  pha
  cpx #$01        ; Check row 0
  bne chk_001
  and #$2c        ; Commodore, run stop and control key
  beq chk_003
  lsr a           ; We found a pressed control key
  sta ctrl_code
  pla
  and #($ff-$2c)  ; Mask out control key bits
  rts
chk_001:
  cpx #$02        ; Check row 1
  bne chk_002
  and #8          ; Left shift key
  beq chk_003
  ora ctrl_code
  sta ctrl_code
  pla
  and #($ff-$08)
  rts
chk_002:
  cpx #$40
  bne chk_003
  and #$10        ; Right shift key
  beq chk_003
  asl a
  ora ctrl_code
  sta ctrl_code
  pla
  and #($ff-$10)
  rts
chk_003:
  pla
  rts

;**********************************************************
; Scan the keyboard and build a table with the detected
; values found on the input port.
; The algorithm scans each keyboard row and for every row
; it stores the column value read back on portb in col_buf.
; NOTE:
; The keyboard matrix uses true values instead of inverted
; values that the C64 uses.
; OUTPUT:
;  fnd_rows   - Number of rows with detected key presses
;  ctrl_code  - Pressed control keys
;**********************************************************
scan_key:
  stz ctrl_code   ; Clear control code
  lda #$01
  ldy #0
sk_001:
  sta ddra
  tax
  pha
  ;jsr kbd_delay_1ms
  lda portb
  jsr chk_control ; Filter out special keys
  ; Store found keys in buffer
  sta col_buf,y   ; Store column value in our buffer
  iny
  pla
  asl a
  bcc sk_001
  rts

;**********************************************************
; Scan and convert to ascii representation
;   If no key was detected return with carry cleared
;   otherwise with carry set and result in ACC and
;**********************************************************
get_raw_key:
  jsr scan_key    ; Scan the keyboard
  ldy #0
gk_010:
  lda col_buf,y
  cmp old_col_buf,y
  bcs gk_020      ; Not much to do if nothing has happened or a new key was pressed.

; Do decoding
  tya
  asl a           ; Multiply with 2
;  and #$0f        ; Wrap to table size
  tax
  lda row_tab,x   ; Get pointer to translation table
  sta rowptr
  lda row_tab+1,x
  sta rowptr + 1

  lda col_buf,y   ; Decode the colum bits
  eor old_col_buf,y

  ldy #0          ; Bit counter
  ldx #0          ; key output buffer pointer
gk_002:
  asl a
  pha
  bcc gk_003
  ; Take care of key press
  lda (rowptr),y
  sta key_out,x
  inx
  cpx #OUT_KEYS   ; Reached end of buffer
  beq gk_004      ; Yes, end here
gk_003:
  pla
  iny
  cpy #8
  bne gk_002
gk_004:
  jsr xfr_col_buf

  ; Shift key handling
  lda ctrl_code
  and #(lshift_key + rshift_key)
  beq gk_025

  lda key_out
  cmp #'1'
  bcc gk_025
  cmp #':'
  bcs gk_025

  sec
  sbc #$10
  sta key_out

gk_025:
  ; Control key handling
  lda ctrl_code
  and #ctrl_key
  beq gk_030

  lda key_out
  cmp #'C'
  bne gk_030
  lda #3
  sta key_out

gk_030:
  sec
  rts

; Next row
gk_020:
  iny
  cpy #8
  bne gk_010      ; Check next row

get_key_end:
; Transfer new column buffers to old buffer
  jsr xfr_col_buf

  clc
  rts

;**********************************************************
; Raw key wrapper
;**********************************************************
get_key:
  phx
  phy

  jsr get_raw_key

  ply
  plx

  lda key_out

  rts
;**********************************************************
; Transfer current column buffer to old column buffer
;**********************************************************
xfr_col_buf:
  ldy #7
xfr_010:
  lda col_buf,y
  sta old_col_buf,y
  dey
  bpl xfr_010
  jmp kbd_delay_1ms

;**********************************************************
; VIA (W65C22) registers
;**********************************************************
kbd_delay_1ms:
  lda   #$00
  sta   acr      ; select mode
  lda   #.lobyte(TIME_1MS)
  sta   t2cl     ; low-latch=0
  lda   #.hibyte(TIME_1MS)
  sta   t2ch     ; high part=01hex.  start
  lda   #$20     ; mask
loop:
  bit   ifr      ; time out?
  beq   loop
  lda   t2cl     ; clear timer 2 interrupt

  rts

row_tab:  .word row_0, row_1, row_2, row_3
          .word row_4, row_5, row_6, row_7
row_0:    .byte '2', 'Q',   0, ' ',   0,   0, $1B, '1'
row_1:    .byte '4', 'E', 'S', 'Z',   0, 'A', 'W', '3'
row_2:    .byte '6', 'T', 'F', 'C', 'X', 'D', 'R', '5'
row_3:    .byte '8', 'U', 'H', 'B', 'V', 'G', 'Y', '7'
row_4:    .byte '0', 'O', 'K', 'M', 'N', 'J', 'I', '9'
row_5:    .byte '-', '@', ':', '.', ',', 'L', 'P', '+'
row_6:    .byte 126, '1', '=',   0, '/', ';', '*', '$'
row_7:    .byte '4', '3', '2', '1',   1,   2, $0d, $08
