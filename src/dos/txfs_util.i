;******************************************************************************
;*
;*  TX8 8-bit color computer
;*
;*  txfs_util.i
;*    Assortment of utilities for the TXFS file system
;*
;******************************************************************************

.import     add_LBA32
.import     inc_lba_pointer
.import     add_lba32_pointer
.import     add_lba16_pointer
