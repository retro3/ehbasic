; VDP module wide zeropage variables
    .import         vdp_cnt: zeropage
    .import         vdp_cnt_hi: zeropage
    .import         vdp_cnt_hi2: zeropage
    .import         vdp_curoff: zeropage
    .import         vdp_curstat: zeropage
    .import         vdp_curval: zeropage
    .import         vdp_blank: zeropage
    .import         vdp_delay: zeropage
    .import         tmp_alo: zeropage
    .import         tmp_ahi: zeropage
    .import         tmp_blo: zeropage
    .import         tmp_bhi: zeropage
    .import         tmp_clo: zeropage
    .import         tmp_chi: zeropage
    .import         tmp_a: zeropage
    .import         scratch: zeropage
    .import         ztmp_16: zeropage
    .import         num_a: zeropage
    .import         num_b: zeropage
    .import         num_x: zeropage
    .import         num_tmp: zeropage
    .import         num_buf: zeropage

    .import         vdp_base: zeropage
    .import         gr_scrngeom: zeropage

; VDP module functions
    .import         vdp_font
