;******************************************************************************
;*
;*  TX8 8-bit color computer
;*
;*  inodes.s
;*    Handle inodes and the inode bitmap.
;*
;******************************************************************************
.setcpu     "65c02"

.include    "macros.i"
.include    "txfs.i"
.include    "cf_driver.i"
.include    "txfs_data.i"
.include    "txfs_util.i"

d_ptr       = $00           ; ZP used during boot
s_ptr       = $02           ; Same
i_num       = $04           ; Used to hold inode number

.segment    "CODE"
;******************************************************************************
;* Search for the first free inode
;* The inode bitmap is a linear array of bits where each bit maps to a
;* given inode entry. Searching the array is a simple task of starting
;* at bit zero and scanning the array forward.
;*  ACC -
;*    X -
;*    Y -
;* Returns:
;*  i_node - The first free inode number.
;* Implementation notes:
;* The implementation uses four nested loops.
;* The outer loop reads bitmap blocks from the disk into the disk buffer.
;* Next level loop (X) takes care of looping through two 256 byte blocks.
;* Next level loop (Y) reads individual bytes in the 256 byte block
;* The innermost level shifts bits to find a free inode
;******************************************************************************
get_free_inode:
  ldp   s_ptr, disk_buffer
  ldp   dat_ptr, disk_buffer

  ldy   #0
  sty   i_num
  sty   i_num+1
  ldx   #2                  ; We need to scan 2 256 byte blocks
  load_sect part_info + txfs_partition::inode_bmp_1
gfi_000:
  jsr   cf_read_sector      ; Read boot sector of partition
gfi_001:
  lda   (s_ptr),y           ; Load byte from bitmap
  ; Search for a clear bit by rotating the bits into the carry
  phy                       ; Temporarily save Y register
  ldy   #8                  ; Search all 8 bits
gfi_002:
  lsr   a                   ; Shift bits right
  bcc   gfi_found_free_inode; Found free entry so exit
  inc   i_num               ; Increment inode number
  bne   gfi_003
  inc   i_num+1
gfi_003:
  lda   i_num               ; Check if we reached the end of the inode bitmap
  cmp   part_info + txfs_partition::inode_bmp_len
  bne   gfi_004
  lda   i_num+1
  cmp   part_info + txfs_partition::inode_bmp_len+1
  beq   gfi_err_bmp_full
gfi_004:
  dey
  bne   gfi_002             ; 8 bits ?

  ply                       ; retrieve outer loop index
  iny
  bne   gfi_001             ; All bytes ?
  inc   s_ptr+1
  dex                       ; next page
  bne   gfi_001

  jsr   inc_lba_pointer
  bra   gfi_000             ; Go read next block and continue the search

gfi_err_bmp_full:
  sec                       ; Carry set means no free inode was found
  rts

gfi_found_free_inode:
  clc                       ; We found it, the found inode is in i_num
  rts

;******************************************************************************
;* Allocate inode
;*  ACC -
;*    X -
;*    Y -
;* Implementation notes
;******************************************************************************
allocate_inode:
  jsr   get_free_inode      ; Get first free inode
  bcs   ai_err_bmp_full     ; Exit if no free inode available

  lda   i_num
  and   #$7                 ; Index into byte to bits table
  tay
  lda   byte_bits, y        ; Get appropriate bit
  ora   (s_ptr), y          ; allocate the inode in the  bitmap
  sta   (s_ptr) ,y          ; And store it

  jsr   cf_write_sector     ; Write the new block to disk
  clc
ai_err_bmp_full:
  rts

;******************************************************************************
;* Free inode
;*  ACC -
;*    X -
;*    Y -
;* Other parameters:
;*  i_num - Input node to free up
;* Implementation notes
;* The block is calculated with this formula:
;*   block = i_num / (512 * 8)
;*   This can be simplified by taking the high byte of i_num and simply
;*   rotating this right 4 times to get the relative base block.
;******************************************************************************
free_inode:
  ; First calculate what block in the bitmap we shall address
  lda   i_num+1
  lsr   a
  lsr   a
  lsr   a
  lsr   a
  sta   cflba0
  stz   cflba1
  stz   cflba2
  stz   cflba3

  ldp   s_ptr, part_info + txfs_partition::inode_bmp_1
  jsr   add_lba32_pointer

  ldp   dat_ptr, disk_buffer
  jsr   cf_read_sector      ; Get bitmap sector
