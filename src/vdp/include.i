VDP_BASE        = $FE60
VDP_MODE0       = VDP_BASE
VDP_MODE1       = VDP_BASE+1
VDP_STATUS      = VDP_BASE+1
VDP_ADDR        = VDP_BASE+1
VDP_VRAM        = VDP_BASE
VDP_SHORTDELAY  = 1
VDP_LONGDELAY   = 2
VDP_FLASH       = $10      ;* Must be a power of 2 *

UTF_ETX    = $03      ; Break character
UTF_BEL    = $07
CRSR_LEFT  = $08
CRSR_RIGHT  = $09
CRSR_DOWN  = $0a
CRSR_UP    = $0b
UTF_ACK    = $06      ; Used for the copy key in this implementation
UTF_FF    = $0c
UTF_CR    = $0d
UTF_BRK    = $1a      ; Debug - drop in to monitor
UTF_DEL    = $08
UTF_SPECIAL = $20

;* This structure defines the key information
;* kept about the VDP current mode
.struct vdp_addr_struct
    vdp_addr_nme   dw         ;* Address of name table
    vdp_addr_col   dw         ;* Address of colour table
    vdp_addr_pat   dw         ;* Address of pattern table
    vdp_addr_spa   dw         ;* Address of sprite pattern table
    vdp_addr_spp   dw         ;* Address of sprite position table
    vdp_bord_col   db         ;* Value of border colour
.endstruct

.struct gr_screen
    gr_screen_start dw    ; Start of screen memory in VDP
    gr_screen_size  dw    ; Number of bytes screen occupies
    gr_screen_w     db    ; Number of columns
    gr_screen_h     db    ; Number of rows
    gr_cur_off      db    ; Y offset of cursor image from current position
    gr_cur_x        db    ; Current X position of cursor
    gr_cur_y        db    ; Current Y position of cursor
    gr_cur_ptr      dw    ; VDP address of cursor
    gr_pixmode      db    ; Pixel plot mode (0=Erase, 1=Plot, 2=XOR)
    gr_pixmask      db    ; Pixel plot mask
    gr_pixcol       db    ; Pixel colour
    gr_geom_tmp     dw    ; One word of temp storage for local use
.endstruct

;* Software break to throw errors
;* use like this : SWBRK XX
;* Where XX is the error code
  .macro SWBRK sig
  brk
  db sig
  .endmacro

  .macro _pushAXY
  pha
  phx
  phy
  .endmacro

  .macro _pullAXY
  ply
  plx
  pla
  .endmacro

  .macro _println msg
  _pushAXY
  ldx #<(msg)
  lda #>(msg)
  jsr io_print_line
  _pullAXY
  .endmacro

  .macro _printmsgA msg
  phx
  phy
  pha
  ldx #<(msg)
  lda #>(msg)
  jsr io_print_line
  pla
  pha
  jsr str_a_to_x
  jsr _put_byte
  txa
  jsr _put_byte
  lda #UTF_CR
  jsr _put_byte
  pla
  ply
  plx
  .endmacro

  .macro _printA
  phx
  phy
  pha
  jsr str_a_to_x
  jsr _put_byte
  txa
  jsr _put_byte
  pla
  ply
  plx
  .endmacro

  .macro _printCRLF
  pha
  lda #UTF_CR
  jsr _put_byte
  pla
  .endmacro

  .macro _printC ch
  pha
  lda #ch
  jsr _put_byte
  pla
  .endmacro

  .macro _printCA
  pha
  jsr _put_byte
  pla
  .endmacro

  .macro _sendcmd cmd
  _pushAXY
  ldx #<(cmd)
  lda #>(cmd)
  jsr sd_sendcmd
  _pullAXY
  .endmacro

  .macro _incZPWord wordp
  inc wordp
  db  $d0, $02
  inc wordp+1
  .endmacro

  .macro _decZPWord wordp
  pha
  sec
  lda wordp
  sbc #1
  sta wordp
  lda wordp+1
  sbc #0
  sta wordp+1
  pla
  .endmacro

  .macro _cpyZPWord worda,wordb
  lda worda
  sta wordb
  lda worda+1
  sta wordb+1
  .endmacro

  .macro _addZPWord worda, wordb
  clc
  lda worda
  adc wordb
  sta worda
  lda worda+1
  adc wordb+1
  sta worda+1
  .endmacro

  .macro _subZPWord worda, wordb
  sec
  lda worda
  sbc wordb
  sta worda
  lda worda+1
  sbc wordb+1
  sta worda+1
  .endmacro

  .macro _adcZPWord worda,const
  clc
  lda worda
  adc #const
  sta worda
  lda worda+1
  adc #0
  sta worda+1
  .endmacro
