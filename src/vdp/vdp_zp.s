;**********************************************************
;*
;*  TX8 8-bit home computer
;*
;**********************************************************
  .setcpu         "65C02"
  .define         EQU =
  .define         db  .byte
  .define         dw  .word
  .define         ds  .res

  .include        "include.i"

  .export         vdp_cnt
  .export         vdp_cnt_hi
  .export         vdp_cnt_hi2
  .export         vdp_curoff
  .export         vdp_curstat
  .export         vdp_curval
  .export         vdp_blank
  .export         vdp_delay
  .export         tmp_alo
  .export         tmp_ahi
  .export         tmp_blo
  .export         tmp_bhi
  .export         tmp_clo
  .export         tmp_chi
  .export         tmp_a
  .export         scratch
  .export         ztmp_16
  .export         num_a
  .export         num_b
  .export         num_x
  .export         num_tmp
  .export         num_buf
  .export         vdp_base
  .export         gr_scrngeom

  .zeropage
; From vdp-drvr.asm
vdp_cnt:        .res  1   ; VDP interrupt counter
vdp_cnt_hi:     .res  1   ; VDP counter high
vdp_cnt_hi2:    .res  1   ; VDP counter high 2
vdp_curoff:     .res  1   ; Cursor off (0 = On)
vdp_curstat:    .res  1   ; Cursor status
vdp_curval:     .res  1   ; Cursor value on screen
vdp_blank:      .res  1   ; Screen blank value normally 32
vdp_delay:      .res  1   ; Delay counter for VRAM access

; From graph.asm
tmp_alo:        .res  1     ; VDP addresses lo
tmp_ahi:        .res  1     ; VDP addresses hi
tmp_blo:        .res  1     ; Temp address lo
tmp_bhi:        .res  1     ; Temp address hi
tmp_clo:        .res  1     ; Temp address lo
tmp_chi:        .res  1     ; Temp address hi
tmp_a:          .res  1     ; Temp storage a
scratch:        .res  16    ; Scratch pad area
ztmp_16:
num_a:          .res  4     ; 4 byte primary accumulator
num_b:          .res  4     ; 4 byte secondary accumulator
num_x:          .res  4     ; 4 byte x register
num_tmp:        .res  4     ; 4 byte temp space
num_buf:        .res  8     ; 8 byte string buffer

; vdp settings
vdp_base:       .tag  vdp_addr_struct
; Screen geometry
gr_scrngeom:    .tag  gr_screen
