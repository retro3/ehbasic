;******************************************************************************
;*
;*  TX8 8-bit color computer
;*
;*  cf_driver.i
;*    API for the cf driver
;*
;******************************************************************************
  .import   cf_init
  .import   cf_write_sector
  .import   cf_read_sector
  .import   cf_wait
  .import   cf_check_error
  .import   cf_read_data
  .import   cf_write_data
  .import   cf_set_lba
  .import   cf_info

  .import   dat_ptr: zeropage
  .import   cf_error: zeropage

  .import   cflba3
  .import   cflba2
  .import   cflba1
  .import   cflba0
