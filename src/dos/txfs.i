;******************************************************************************
;*
;*  TX8 8-bit color computer
;*
;*  txfs.i
;*    Data definitions and structures used in the file system
;*
;******************************************************************************

; Structure for the header in the super block
.struct txfs_sb
  id             .res   2   ; Superblock ID               $00
  version        .res   1   ; Superblock version          $02
  no_active      .res   1   ; Number of active partitions $03
  last           .res   4   ; Last block of disk          $04
  label          .res   8   ; Label of disk               $08
.endstruct

; Data structure for the partition information
.struct txfs_pi
  first_sect    .res  4   ; First sector of partition     $10
  length        .res  2   ; Length of partition           $14
  status        .res  2   ; Partition status bits.        $16
.endstruct

; Definition of volume header
; Notes:
; All sector pointers are relative to the first sector of the partition.
; The first_sec pointer allows the partition to allow for the boot block to
; grow beyond its one block it currently has.
; In version 1 of the partition first_sec should always be 1. This means that
; the partition starts directly after the boot sector which is sector 0.
; In case the boot block needs to be expanded the first_sec field can be
; adjusted to make room for that.
; The rest of the block pointers are referenced to the first_sec field.
; To calculate the absolute block of the disk you must add the pointer to
; the first_sec field and the base pointer of the partition.
; Example: To calculate the first block of the first inode region you would
; use the following formula: blk = part_base + first_sec + inode_1
; These values will not change during use so you can pre calculate all values
; when mounting the filesystem.
.struct txfs_partition
  magic         .res 4    ; Magic number for the file system (TXFS)
  version       .res 1    ; Version of file system
  first_sect    .res 4    ; The first sector of this filesystem
  last_sect     .res 4    ; The last sector of the filesystem
  id            .res 2    ; Numeric ID of the partition
  label         .res 32   ; Text description of partition
  inode_bmp_1   .res 4    ; First logical block of inode bitmap
  inode_bmp_len .res 1    ; Length of inode bitmap (number of inodes)
  data_bmp_1    .res 4    ; First logical block of data bitmap
  data_bmp_len  .res 1    ; Length of data bitmap (number of blocks)
  inodes_1      .res 4    ; First logical block of inode region
  inodes_len    .res 2    ; Length of inode region (in blocks)
  data_1        .res 4    ; First logical block of user data region
  data_len      .res 2    ; Length of user data region (in blocks)
  status        .res 2    ; Status bits
.endstruct

; Structure for Inodes
.struct txfs_inode
  mode          .res 2    ; Access rights
  size          .res 4    ; Number of bytes in file
  time          .res 4    ; The last time this file was accessed
  ctime         .res 4    ; Time of creation
  mtime         .res 4    ; The last time the file was modified
  dtime         .res 4    ; Time the file was deleted
  blocks        .res 2    ; How many blocks this file has allocated
  flags         .res 2    ; Instruction flags
  block         .res 4    ; Pointer to the first block in the data chain
  empty         .res 2    ; Reserved storage
.endstruct

max_parts   = 62          ; There is only room for 62 partitions (64 - 2)
part_offset = $10         ; Offset to partition list
inode_size  = $20         ; Number of bytes in an inode entry
txfs_pi_size = $08        ; Size og partition entry

disk_buffer = $0400       ; System diskbuffer
