; API for graph.asm

  .import   gr_put_byte
  .import   gr_get_key
  .import   gr_del
  .import   gr_new_ln
  .import   gr_cur_down
  .import   gr_cur_up
  .import   gr_cur_left
  .import   gr_cur_right
  .import   gr_scroll_up
  .import   gr_line
  .import   gr_circle
  .import   gr_box
  .import   gr_point
  .import   gr_hchar
  .import   gr_set_cur
  .import   gr_get
  .import   gr_put
  .import   gr_plot
  .import   gr_getXY_ptr
  .import   gr_cls
  .import   gr_init_screen
  .import   gr_init_hires
  .import   gr_init_screen_txt
  .import   gr_init_screen_g
  .import   gr_init_screen_common
